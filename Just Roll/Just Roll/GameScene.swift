//
//  GameScene.swift
//  Just Roll
//
//  Created by shreyas shekhar on 5/17/17.
//  Copyright © 2017 shreyas. All rights reserved.
//

import SpriteKit
import GameplayKit

struct PhysicsCategory {
    static let None      : UInt32 = 0
    static let All       : UInt32 = UInt32.max
    static let ball      : UInt32 = 1      // 1
    static let bar       : UInt32 = 2      // 2
    static let endLine   : UInt32 = 3      // 3
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

#if !(arch(x86_64) || arch(arm64))
    func sqrt(a: CGFloat) -> CGFloat {
        return CGFloat(sqrtf(Float(a)))
    }
#endif

extension CGPoint {
    func length() -> CGFloat {
        return sqrt(x*x + y*y)
    }
    
    func normalized() -> CGPoint {
        return self / length()
    }
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var score = 0
    private var lives = 3
    
    var firstTouch = true
    override func didMove(to view: SKView) {
        //Setting the background and scene
        backgroundColor = SKColor.white
        physicsWorld.gravity.dy  = -1
        physicsWorld.contactDelegate = self
        
        //creating bars
        run(SKAction.repeatForever(
            SKAction.sequence([
                SKAction.run(addBar3),
                SKAction.run(addBar2),
                SKAction.run(addBar1),
                SKAction.run(addBar4),
                SKAction.wait(forDuration: 1.0)
                ])
        ))
        
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    func updateScore(value : Int){
        // Updating score count
        let label = childNode(withName: "scoreCount") as! SKLabelNode
        score += value
        label.text = "Score:" + String(score)
    }
    func remove_heart (value : String){
        // removing heart
        lives -= 1
        let heart = self.childNode(withName: value)
        heart?.removeFromParent()
    }
    
    func addBar3() {
        
        // Create sprite
        let bar = SKSpriteNode(imageNamed: "Bar 3")
        
        
        bar.physicsBody = SKPhysicsBody(rectangleOf: bar.size) // 1
        bar.physicsBody?.isDynamic = true // 2
        bar.physicsBody?.categoryBitMask = PhysicsCategory.bar // 3
        bar.physicsBody?.contactTestBitMask = PhysicsCategory.ball // 4
        bar.physicsBody?.collisionBitMask = PhysicsCategory.None // 5
        bar.physicsBody?.affectedByGravity = false;
        
        // Determine where to spawn the bar along the X and Y axis
        let actualY = bar.size.height*0.75
        let actualX = random(min: CGFloat(450.0), max: CGFloat(500.0))
        bar.position = CGPoint(x: actualX, y: actualY)
        
        // Add the bar to the scene
        addChild(bar)
        
        // Determine speed of the bar
        let actualDuration = 2.0
        
        // Create the actions
        let actionMove = SKAction.move(to: CGPoint(x: -bar.size.width, y: actualY), duration: TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        bar.run(SKAction.sequence([actionMove, actionMoveDone]))
        
    }
    func addBar2() {
        
        // Create sprite
        let bar = SKSpriteNode(imageNamed: "Bar 2")
        
        bar.physicsBody = SKPhysicsBody(rectangleOf: bar.size) // 1
        bar.physicsBody?.isDynamic = true // 2
        bar.physicsBody?.categoryBitMask = PhysicsCategory.bar // 3
        bar.physicsBody?.contactTestBitMask = PhysicsCategory.ball // 4
        bar.physicsBody?.collisionBitMask = PhysicsCategory.None // 5
        bar.physicsBody?.affectedByGravity = false;
        
        // Determine where to spawn the monster along the X and Y axis
        let actualY = bar.size.height*0.35
        let actualX = random(min: CGFloat(950.0), max: CGFloat(1000.0))
        bar.position = CGPoint(x: actualX, y: actualY)

        
        // Add the bar to the scene
        addChild(bar)
        
        // Determine speed of the bar
        let actualDuration = 4.0
        // Create the actions
        let actionMove = SKAction.move(to: CGPoint(x: -bar.size.width, y: actualY), duration: TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        bar.run(SKAction.sequence([actionMove, actionMoveDone]))
        
    }
    func addBar1() {
        
        // Create sprite
        let bar = SKSpriteNode(imageNamed: "Bar 1")
        
        bar.physicsBody = SKPhysicsBody(rectangleOf: bar.size) // 1
        bar.physicsBody?.isDynamic = true // 2
        bar.physicsBody?.categoryBitMask = PhysicsCategory.bar // 3
        bar.physicsBody?.contactTestBitMask = PhysicsCategory.ball // 4
        bar.physicsBody?.collisionBitMask = PhysicsCategory.None // 5
        bar.physicsBody?.affectedByGravity = false;
        
        // Determine where to spawn the monster along the Y axis
        let actualY = bar.size.height*(-0.05)
        let actualX = random(min: CGFloat(2150.0), max: CGFloat(2200.0))
        bar.position = CGPoint(x: actualX, y: actualY)

        
        // Add the bar to the scene
        addChild(bar)
        
        // Determine speed of the bar
        let actualDuration = 6.0
        
        // Create the actions
        let actionMove = SKAction.move(to: CGPoint(x: -bar.size.width, y: actualY), duration: TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        bar.run(SKAction.sequence([actionMove, actionMoveDone]))
    }
    func addBar4() {
        
        // Create sprite
        let bar = SKSpriteNode(imageNamed: "Bar 4")
        
        bar.physicsBody = SKPhysicsBody(rectangleOf: bar.size) // 1
        bar.physicsBody?.isDynamic = true // 2
        bar.physicsBody?.categoryBitMask = PhysicsCategory.bar // 3
        bar.physicsBody?.contactTestBitMask = PhysicsCategory.ball // 4
        bar.physicsBody?.collisionBitMask = PhysicsCategory.None // 5
        bar.physicsBody?.affectedByGravity = false;
        
        
        // Determine where to spawn the monster along the X and Y axis
        let actualY = bar.size.height*(-0.45)
        let actualX = random(min: CGFloat(3950.0), max: CGFloat(4000.0))
        bar.position = CGPoint(x: actualX, y: actualY)

        
        // Add the bar to the scene
        addChild(bar)
        
        // Determine speed of the bar
        let actualDuration = 8.0
        
        // Create the actions
        let actionMove = SKAction.move(to: CGPoint(x: -bar.size.width, y: actualY), duration: TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        bar.run(SKAction.sequence([actionMove, actionMoveDone]))
    }
    
    func ballReachedTheEndLine(blueBall: SKSpriteNode, endLine: SKSpriteNode) {
        print("Game Over")
        blueBall.removeFromParent()
        remove_heart(value: "heart3")
        
    }
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        if firstBody.categoryBitMask == 1 {
            if secondBody.categoryBitMask == 3{
                print("command reached here\n");
                let blueBall = firstBody.node as? SKSpriteNode
                let endLine = secondBody.node as? SKSpriteNode
                ballReachedTheEndLine(blueBall: blueBall! , endLine: endLine!)
            }
        }

    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if firstTouch{
            //adding physics body
            let ballBody = SKPhysicsBody(circleOfRadius: 30)
            ballBody.mass = 1.5
            ballBody.categoryBitMask = PhysicsCategory.ball
            ballBody.collisionBitMask = 4
            ballBody.isDynamic = true
            ballBody.affectedByGravity = true
            ballBody.usesPreciseCollisionDetection = true
            let blueBall = childNode(withName: "blueBall") as! SKSpriteNode
            blueBall.physicsBody = ballBody
            firstTouch = false
            
            let endLineBody = SKPhysicsBody(circleOfRadius: 30)
            endLineBody.categoryBitMask = PhysicsCategory.endLine
            endLineBody.collisionBitMask = 4
            endLineBody.isDynamic = true
            endLineBody.affectedByGravity = false
            endLineBody.usesPreciseCollisionDetection = true
            let endLine = childNode(withName: "endLine") as! SKSpriteNode
            endLine.physicsBody = endLineBody
            
        }
        else{
            // jumping code
        }
        
    }
}

